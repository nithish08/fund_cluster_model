
# coding: utf-8

# In[2]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import warnings
from config_dict import config_dict

warnings.filterwarnings('ignore')


df_pl = pd.read_excel(config_dict['init_plbs_file'])

df_pl['Year'] = df_pl['[Year End'].apply(lambda x:  int(str(int(x))[:4]))
df_pl['Month'] = df_pl['[Year End'].apply(lambda x:  int(str(int(x))[4:]))


FROM_YEAR = 2000


print('total data', df_pl.shape)
df_pl = df_pl[df_pl.Year >= FROM_YEAR]
print('from 2000 year', df_pl.shape)

print('before drop duplicates', df_pl.shape)
df_pl.drop_duplicates(inplace=True)
print('after drop duplicates', df_pl.shape)


# In[8]:


df_bs = pd.read_excel('../data/raw_data/new_bs_autosect_data.xls')




df_bs.fillna(0, inplace=True)


df_bs['Year'] = df_bs['[Year End'].apply(lambda x:  int(str(int(x))[:4]))
df_bs['Month'] = df_bs['[Year End'].apply(lambda x:  int(str(int(x))[4:]))



print('total data', df_bs.shape)
df_bs = df_bs[df_bs.Year >= FROM_YEAR]
print('from 2000 year', df_bs.shape)



print('before drop duplicates', df_bs.shape)
df_bs.drop_duplicates(inplace=True)
print('after drop duplicates', df_bs.shape)


df_agg = pd.merge(df_bs, df_pl, on=['CO_NAME', 'Year', 'Month'], how='left')


# In[33]:


df_agg.to_excel(config_dict['gen_plbs_file'], index=False)
