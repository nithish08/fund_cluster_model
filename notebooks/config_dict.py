

## Pharma Sect
#config_dict = {'init_ratios_file': '../data/raw_data/Pharma_comps/pharma_ratios.xlsx',
#              'init_promoters_file': '../data/../data/raw_data/Pharma_comps/pharma_promoters.xlsx',
#              'init_plbs_file': '../data/raw_data/Pharma_comps/pharma_pl.xlsx',
#               'init_bs_file': '../data/raw_data/Pharma_comps/pharma_bs.xls',
#              'gen_plbs_file' : '../data/inter_data/pharma_pl_top100.xlsx',
 #             'gen_ratios_file' : '../data/inter_data/pharma_final_ratios.xlsx',
  #             'final_file' : '../data/final_data/ratios_plbs_agg_pharma.xlsx',
   #            'final_pickle_file' : '../data/final_data/pharma_company_data_dict.pkl',
    #           'final_col_names':'../data/final_data/pharma_col_names_list.pkl'
     #          }



## Auto Sect
config_dict = {'init_ratios_file': '../data/raw_data/auto_sect_ratios_new_crted.xlsx',
              'init_promoters_file': '../data/../data/raw_data/promoters_holdings_value_auto_sect.xlsx',
              'init_plbs_file': '../data/raw_data/new_pl_autosect_data.xls',
               'init_bs_file': '../data/raw_data/new_bs_autosect_data.xls',
              'gen_plbs_file' : '../data/inter_data/auto_pl_top100.xlsx',
              'gen_ratios_file' : '../data/inter_data/auto_final_ratios.xlsx',
               'final_file' : '../data/final_data/ratios_plbs_agg_auto.xlsx',
               'final_pickle_file' : '../data/final_data/auto_company_data_dict.pkl',
               'final_col_names':'../data/final_data/auto_col_names_list.pkl'
              }

