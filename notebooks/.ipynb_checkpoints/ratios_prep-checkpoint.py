
# coding: utf-8

# In[215]:


import pandas as pd
import warnings
import matplotlib.pyplot as plt
import numpy as np
warnings.filterwarnings('ignore')
from config_dict import config_dict

## input raw ratios file
df = pd.read_excel(config_dict['init_ratios_file'])


all_companies = df.Co_Name.unique()


df.dropna(subset=['Year End'], inplace=True)


df['Year'] = df['Year End'].apply(lambda x: int(str(int(x))[:4]))

df['Month'] = df['Year End'].apply(lambda x: int(str(int(x))[4:]))



FROM_YEAR = 2000

print("df shape before using from year", df.shape)
df = df[df.Year >= FROM_YEAR]
print("df shape after using from year", df.shape)


freq_df = df.Co_Name.value_counts()


MIN_NUM_OF_DATAPOINTS = 10


com_names = freq_df[freq_df >= MIN_NUM_OF_DATAPOINTS].index


print('num of companies before subsetting companies based on the min data points',
      df['Co_Name'].nunique())
df = df[df['Co_Name'].isin(com_names)]
print('num of companies with data points greater than {0} are {1}'.format(
    MIN_NUM_OF_DATAPOINTS, df['Co_Name'].nunique()))


df.fillna(0, inplace=True)



df['Quick Ratio'] = (df['Total Current Assets'] - df['Inventories']) / \
    (df['Current Liabilities'] - df['Bank Overdraft / Short term credit'])

df['Net_Profit_Margin'] = df['PBIT'] / df['Net Sales']

df['Effective_Tax_Rate'] = df['Tax']/df['PBT']

df['ROA'] = df['PAT'] / df['Total Assets']

df['Fixed_Asset_Turnover'] = df['Gross Sales'] / \
    (df['Gross Block'] - df['TOTAL REVALUATION RESERVE'])

df['Inventory_Turnover_Ratio'] = df['Gross Sales'] / df['Inventories']

df['Payables_Turnover_Ratio'] = df['Gross Sales'] / df['Sundry Debtors']

df['Debt_to_Assets_Ratio'] = df['Total Debt'] / df['Total Assets']

df['Debt_to_Capital_Ratio'] = df['Total Debt'] / df['Share Capital']


## read the promoter file here
promoter_df  = pd.read_excel(config_dict['init_promoters_file'])

promoter_df['Year'] = promoter_df['YearampMonth'].apply(
    lambda x: int(str(int(x))[:4]))
promoter_df['Month'] = promoter_df['YearampMonth'].apply(
    lambda x: int(str(int(x))[4:]))


promoter_df = promoter_df[promoter_df.Year >= FROM_YEAR]

promoter_df = promoter_df[['Co_Name', 'Year', 'Month', 'SH Value']]

promoter_df.drop_duplicates(inplace=True)

print('df shape before promoter holdings value merge is', df.shape)

df = pd.merge(df, promoter_df, on=['Co_Name', 'Year', 'Month'], how='left')


print('df shape after PHV merge is ', df.shape)


df.fillna(0, inplace=True)


df.to_excel(config_dict['gen_ratios_file'])