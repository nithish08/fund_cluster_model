
# coding: utf-8

# In[1]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import warnings

warnings.filterwarnings('ignore')


df_ratios = pd.read_excel(config_dict['gen_ratios_file']) # ratios file

df_fund = pd.read_excel(config_dict['gen_plbs_file']) # bs and pl file


df_ratios.columns

df_ratios.rename(columns={'Co_Name': 'CO_NAME'}, inplace=True)


df_agg = pd.merge(df_ratios, df_fund, on=['CO_NAME', 'Year', 'Month'])

for ind, i in enumerate(df_agg.columns):
    print(ind, i)


ind_list = [0, 1, 2, 3, 8, 9, 10, 11, 12, 13, 18, 19, 20, 21, 22, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 40, 41, 42, 43,
            51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87]


col_list = [df_agg.columns[i] for i in ind_list]
df_agg = df_agg[col_list]

df_agg = df_agg.dropna()

df_agg.to_excel(
    config_dict['final_file'], index=False) ## save the final file here
