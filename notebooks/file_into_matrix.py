
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import pickle

from config_dict import config_dict

df = pd.read_excel(config_dict['gen_ratios_file'])

non_features = ['Co_Code', 'CO_NAME', 'Year End', 'Year', 'Month']

features = [i for i in df.columns if i not in non_features]

with open(config_dict['final_col_names'], 'wb') as f:
    pickle.dump(features, f)


print(df.columns)

# removing companies with less than 10 datapoints
freq_df = df.Co_Name.value_counts()
com_names = freq_df[freq_df >= 10].index
print('shape before filtering companies', df.shape)
df = df[df.Co_Name.isin(com_names)]
print('shape after filtering companies', df.shape)

df.replace([np.inf, -np.inf], np.nan, inplace=True)

# removing the duplicates
print('shape before removing the duplicates ', df.shape)
df.drop_duplicates(inplace=True)
print('shape after removing the duplicates', df.shape)

sub = df[df.Co_Name == df.Co_Name.unique()[-1]]

# removing the na values
# removing the duplicates
print('shape before removing the na ', df.shape)
df.dropna(inplace=True)
print('shape after removing the na', df.shape)

com_list = df.Co_Name.unique()

mat_list = []

for cmpy in com_list:
    sub = df[df.Co_Name == cmpy]
    sub.sort_values(['Year', 'Month'], inplace=True)
#     print(sub.Year.tolist())
#     print(sub.Month.tolist())
    mat_list.append(sub[features].values)

store_dict = dict((i, j) for i, j in zip(com_list, mat_list))


with open(config_dict['final_pickle_file'], 'wb') as f:
    pickle.dump(store_dict, f)
