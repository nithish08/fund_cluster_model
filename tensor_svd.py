from tensorly.decomposition._tucker import partial_tucker
## read the docs of tensorly for more tensor decompositions....

#input_tensor = (100,20,100)
# mode = axis on which you want reduce dimension
# rank is the final dimension
core,factors = partial_tucker(input_tensor,modes=[2],rank=[45])

#out here will have shape (100,20,45) -> since only 2 axis is reduced to 45



#reconstructing the original tensor can be done as 

recons_tensor= core.dot(factors[0].T)